package main

import (
	"fmt"
	"net/http"

	//"strconv"

	"github.com/gin-gonic/gin"
)


func GenerateResponse(c *gin.Context, req EmployeeDataResponse, resp_code string, resp_desc string){
	c.JSON(http.StatusOK, gin.H{
		"employee_id": req.EmployeeID,
		"company_id": req.CompanyID,
		"division_name": req.DivisionName,
		"division_code": req.DivisionCode,
		"employee_name": req.EmployeeName,
		"employee_birth_date": req.EmployeeBirthDate,
		"employee_nik": req.EmployeeNIK,
		"employee_gender": req.EmployeeGender,
		"employee_band_position": req.EmployeeBandPosition,
		"employee_position": req.EmployeePosition,
		"employee_unit": req.EmployeeUnit,
		"employee_photo": req.EmployeePhoto,
		"employee_email": req.EmployeeEmail,
		"employee_password": req.EmployeePassword,
		"created_by": req.CreatedBy,
		"created_date": req.CreatedDate,
		"last_modified_by": req.LastModifiedBy,
		"last_modified_date": req.LastModifiedDate,
		"is_deleted": req.IsDeleted,
		"personel_subarea_id": req.PersonelSubAreaID,
	})
}
// Message is a utility function to generate the json string message.
func Message(req EmployeeDataResponse) string {
	return fmt.Sprintf(
		`{ "employee_id": %q, "company_id": %q, "division_name": %q, "division_code": %q, "employee_name": %q, "employee_birth_date": %q, "employee_nik": %q, "employee_gender": %q, "employee_band_position": %q, "employee_position": %q, "employee_unit": %q, "employee_photo": %q, "employee_email": %q, "employee_password": %q, "created_by": %q, "created_date": %q, "last_modified_by": %q, "last_modified_date": %q, "is_deleted": %q, "personel_subarea_id": %q }`,
		uint64(req.EmployeeID),
		uint64(req.CompanyID),
		req.DivisionName,
		req.DivisionCode,
		req.EmployeeName,
		req.EmployeeBirthDate,
		req.EmployeeNIK,
		req.EmployeeGender,
		req.EmployeeBandPosition,
		req.EmployeePosition,
		req.EmployeeUnit,
		req.EmployeePhoto,
		req.EmployeeEmail,
		req.EmployeePassword,
		req.CreatedBy,
		req.CreatedDate,
		req.LastModifiedBy,
		req.LastModifiedDate,
		bool(req.IsDeleted),
		uint64(req.PersonelSubAreaID))
}