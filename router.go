package main

import (
	"github.com/gin-gonic/gin"
)

//InitRouter is used for initiate router
func InitRouter() *gin.Engine {
	router := gin.New()
	// router := gin.Default()

	//Get From Static Data Struct
	router.GET("/api/albums", getAlbums)

	//Get From Database
	router.GET("/api/employee", GetEmployeeData)

	return router
}