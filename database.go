package main

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// Db variable
var Db *gorm.DB
var err error

func InitDb() (*gorm.DB, error) {
	dsn := "host=localhost user=postgres password=P@ssw0rd dbname=postgres port=5432 sslmode=disable TimeZone=Asia/Shanghai"
	Db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	if err != nil {
		fmt.Println("Connection Database Error ", err.Error())
	} else {
		//Db.DB().SetMaxIdleConns(2)
		// Db.DB().SetMaxOpenConns(1000)
		// Db.LogMode(true)
		fmt.Println("Connected")
	}

	return Db, err
}

func Connect() (*gorm.DB, error) {
	return Db, err
}
