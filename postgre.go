package main

import (
	"fmt"
)

func GetEmployeeDataDB() ([]EmployeeDataResponse, error) {
	var employeeDataResponseBody []EmployeeDataResponse

	//Connect To HBASE Phoenix DB
	var db, err = Connect()
	//Check Connection
	if err != nil {
		fmt.Println("1")
		fmt.Println(err.Error())
		return nil, err
	}

	err = db.
		Table("public.master_employee").
		Select("*").
		Scan(&employeeDataResponseBody).Error
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	return employeeDataResponseBody, nil
}