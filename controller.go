package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// getAlbums responds with the list of all albums as JSON.
func getAlbums(c *gin.Context) {
    c.IndentedJSON(http.StatusOK, albums)
}

//Employee Data Hadler, Get Employee Data, Using Method GET and URL Param
func GetEmployeeData(c* gin.Context) {

	//Call Data Response From Controller
	message, err := GetEmployeeDataDB()
	if len(message) == 0 {
		c.JSON(http.StatusOK, gin.H{"error": "no data found"})
	} else if err == nil {
		GenerateResponse(c, message[0], "200", "successfully getting ceria data")
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"error": err})
	}
}