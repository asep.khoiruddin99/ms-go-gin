package main

// album represents data about a record album.
type Album struct {
    ID     string  `json:"id"`
    Title  string  `json:"title"`
    Artist string  `json:"artist"`
    Price  float64 `json:"price"`
}

//DataRequest is request for filtering data service
type DataRequest struct {
	Filter string `json:filter`
}

//EmployeeDataResponse is result for employee data service
type EmployeeDataResponse struct {
	EmployeeID           uint64    `json:"employee_id" gorm:"column:employee_id"`
	CompanyID            uint64    `json:"company_id" gorm:"column:company_id"`
	DivisionName         string    `json:"division_name" gorm:"column:division_name"`
	DivisionCode         string    `json:"division_code" gorm:"column:division_code"`
	EmployeeName         string    `json:"employee_name" gorm:"column:employee_name"`
	EmployeeBirthDate    string    `json:"employee_birth_date" gorm:"column:employee_birth_date"`
	EmployeeNIK          string    `json:"employee_nik" gorm:"column:employee_nik"`
	EmployeeGender       string    `json:"employee_gender" gorm:"column:employee_gender"`
	EmployeeBandPosition string    `json:"employee_band_position" gorm:"column:employee_band_position"`
	EmployeePosition     string    `json:"employee_position" gorm:"column:employee_position"`
	EmployeeUnit         string    `json:"employee_unit" gorm:"column:employee_unit"`
	EmployeePhoto        string    `json:"employee_photo" gorm:"column:employee_photo"`
	EmployeeEmail        string    `json:"employee_email" gorm:"column:employee_email"`
	EmployeePassword     string    `json:"employee_password" gorm:"column:employee_password"`
	CreatedBy            string    `json:"created_by" gorm:"column:created_by"`
	CreatedDate          string    `json:"created_date" gorm:"column:created_date"`
	LastModifiedBy       string    `json:"last_modified_by" gorm:"column:last_modified_by"`
	LastModifiedDate     string    `json:"last_modified_date" gorm:"column:last_modified_date"`
	IsDeleted            bool      `json:"is_deleted" gorm:"column:is_deleted"`
	PersonelSubAreaID    uint64    `json:"personel_subarea_id" gorm:"column:personel_subarea_id"`
}